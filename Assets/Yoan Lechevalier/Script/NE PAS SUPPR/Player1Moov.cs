﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;

public class Player1Moov : MonoBehaviour
{
    public Rigidbody2D rigi;

    public float formFSpeed;

    public float jumpForce1;

    public float jumpForceWall1;

    public float wallSpeed1;

    public Vector2 wallJump;
    
    
    void Start()
    {
        rigi = transform.root.gameObject.GetComponent<Rigidbody2D>();
        jumpForceWall1 = 10;
    }
    
    void Update()
    {
        if (wallJump.x > 0 && wallJump.y > 0)
        {
            wallJump.x -= Time.deltaTime * 5;
            wallJump.y -= Time.deltaTime * 5;
        }
        else
        {
            wallJump.x += Time.deltaTime * 5;
            wallJump.y += Time.deltaTime * 5;
        }
    }

    public void Moov1()
    {
        float direction = Input.GetAxis("Horizontal");
        
        transform.root.gameObject.transform.Translate(new Vector3(formFSpeed * direction, 0,0));
         
         if (transform.root.gameObject.GetComponent<MoovDisplay>().grounded)
         {
             rigi.velocity = new Vector2(0, rigi.velocity.y);
         }

         /*if (!transform.parent.gameObject.GetComponent<MoovDisplay>().onWallLeft && transform.parent.gameObject.GetComponent<MoovDisplay>().onWallRight)
         {
             
         }*/
    }

    public void Jump1()
    {
        if (transform.parent.gameObject.GetComponent<MoovDisplay>().grounded)  //saut sur le sol
        {
            rigi.velocity = new Vector2(rigi.velocity.x, jumpForce1);
        }
        else if (transform.parent.gameObject.GetComponent<MoovDisplay>().onWallLeft)  //saut contre un mur
        {
            rigi.AddForce(new Vector2(15,20), ForceMode2D.Impulse);
        }
        else if (transform.parent.gameObject.GetComponent<MoovDisplay>().onWallRight)
        {
            rigi.AddForce(new Vector2(-15,20), ForceMode2D.Impulse);
        }
        else if (transform.parent.gameObject.GetComponent<MoovDisplay>().bonusJump > 0)  // saut dans les air
        {
            rigi.velocity = Vector2.up * jumpForce1;
            transform.parent.gameObject.GetComponent<MoovDisplay>().bonusJump--;
        }
        
    }

    public void SlideWall1()
    {
        if (rigi.velocity.y < -wallSpeed1 &&  transform.parent.gameObject.GetComponent<MoovDisplay>().onWallLeft || rigi.velocity.y < -wallSpeed1 &&  transform.parent.gameObject.GetComponent<MoovDisplay>().onWallRight) 
        {
            rigi.velocity = new Vector2(rigi.velocity.x, -wallSpeed1);
        }
    }
}
