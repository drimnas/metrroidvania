﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class MoovDisplay : MonoBehaviour
{
    public GameObject gears;

    public GameObject formT;
    public GameObject formF;
    public SpriteRenderer playerSprite;
    public Sprite formTSprite;
    public Sprite formFSprite;
    public Color baseColor;
    public Rigidbody2D rigi;

    public bool form;
    public bool onWallLeft;
    public bool onWallRight;
    public bool facingLeft;
    public bool jumping;
    public bool grounded;

    public Vector3 baseScale;
    
    public int bonusJump;
    public int baseJump;
    
    public Transform feets;


    void Start()
    {
        rigi = GetComponent<Rigidbody2D>();
        gears = GameObject.Find("GearsTest");
        playerSprite = GetComponent<SpriteRenderer>();
        baseColor = playerSprite.color;
        baseScale = transform.localScale;
    }
    
    void Update()
    {
        if (form)
        {
            formT.GetComponent<PlayerMoov2>().Moov2();
            formT.GetComponent<PlayerMoov2>().SlideWall2();
            rigi.mass = 3.5f;
            playerSprite.sprite = formTSprite;
        }
        else
        {
            formF.GetComponent<Player1Moov>().Moov1();
            formF.GetComponent<Player1Moov>().SlideWall1();
            rigi.mass = 2;
            playerSprite.sprite = formFSprite;
        }

        if (Input.GetButtonDown("Jump"))
        {
            if (form)
            {
                formT.GetComponent<PlayerMoov2>().Jump2();
            }
            else
            {
                formF.GetComponent<Player1Moov>().Jump1();
            }
        }

        if (grounded)
        {
            bonusJump = baseJump;
        }

        if (Input.GetAxis("Horizontal") > 0)
        {
            facingLeft = false;
            transform.localScale = new Vector3(baseScale.x, transform.localScale.y, transform.localScale.z);
        }
        if (Input.GetAxis("Horizontal") < 0)
        {
            facingLeft = true;
            transform.localScale = new Vector3(-baseScale.x, transform.localScale.y, transform.localScale.z);
        }
        
        grounded = Physics2D.OverlapCircle(feets.position, 0.1f, gears.GetComponent<GearsScript>().platform);
        onWallLeft = Physics2D.OverlapCircle(transform.position + new Vector3(-0.2f,0.2f, 0), 0.3f, gears.GetComponent<GearsScript>().platform);
        //onWallLeft = Physics2D.Raycast(transform.position, Vector2.left,3, gears.GetComponent<GearsScript>().platform);
        onWallRight = Physics2D.OverlapCircle(transform.position + new Vector3(0.2f,0.2f, 0), 0.3f, gears.GetComponent<GearsScript>().platform);
        //onWallLeft = Physics2D.Raycast(transform.position, Vector2.right,3, gears.GetComponent<GearsScript>().platform);

        if (Input.GetButtonDown("Fire2"))//test
        {
            TakeDamage(gameObject);
        }
    }

    public void TakeDamage(GameObject go)
    {
        if (playerSprite.color == baseColor)
        {
            rigi.AddForce(go.transform.right * 5000);// changer le "* 5000" par la puisssance dans un script du gameobject
            playerSprite.DOColor(Color.red, 1).OnComplete(ReturnBaseColor);
        }
    }

    public void ReturnBaseColor()
    {
        playerSprite.color = baseColor;
    }
}
