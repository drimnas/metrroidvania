﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMoov2 : MonoBehaviour
{
    public Rigidbody2D rigi;

    public float formTSpeed;

    public float jumpForce2;

    public float jumpForceWall2;

    public float wallSpeed2;

    public float acceleration;
    
    void Start()
    {
        rigi = transform.root.gameObject.GetComponent<Rigidbody2D>();
        jumpForceWall2 = 15;
    }
    
    void Update()
    {
        
    }

    public void Moov2()
    {
        float direction = Input.GetAxis("Horizontal");

        if (direction != 0)
        {
            if (formTSpeed < 0.15f)
            {
                formTSpeed += Time.deltaTime * acceleration;
            }
        }
        else
        {
            formTSpeed = 0;
        }
        
        transform.root.gameObject.transform.Translate(new Vector3(formTSpeed * direction, 0,0));
         
        if (transform.root.gameObject.GetComponent<MoovDisplay>().grounded)
        {
            rigi.velocity = new Vector2(0, rigi.velocity.y);
        }
    }
    
    public void Jump2()
    {
        if (transform.parent.gameObject.GetComponent<MoovDisplay>().grounded)
        {
            rigi.velocity = new Vector2(rigi.velocity.x, jumpForce2);
        }
        else if (transform.parent.gameObject.GetComponent<MoovDisplay>().onWallLeft)
        {
            rigi.AddForce(new Vector2(25,30), ForceMode2D.Impulse);
        }
        else if (transform.parent.gameObject.GetComponent<MoovDisplay>().onWallRight)
        {
            rigi.AddForce(new Vector2(-25,30 ), ForceMode2D.Impulse);
        }
        else if (transform.parent.gameObject.GetComponent<MoovDisplay>().bonusJump > 0)
        {
            rigi.velocity = Vector2.up * jumpForce2;
            transform.parent.gameObject.GetComponent<MoovDisplay>().bonusJump--;
        }
    }
    
    public void SlideWall2()
    {
        if (rigi.velocity.y < -wallSpeed2 &&  transform.parent.gameObject.GetComponent<MoovDisplay>().onWallLeft || rigi.velocity.y < -wallSpeed2 &&  transform.parent.gameObject.GetComponent<MoovDisplay>().onWallRight) 
        {
            rigi.velocity = new Vector2(rigi.velocity.x, -wallSpeed2);
        }
    }
}
