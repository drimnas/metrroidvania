﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ChangementDeForm : MonoBehaviour
{
    public GameObject gears;
    public GameObject player;

    public LayerMask playerLayer;
    public bool playerInRange;
    public bool textShowed;

    public GameObject showTextToThePlayer;
    public GameObject textInstantiated;
    public Transform showTextHere;

    public float timeWard1 = 21;
    public float timeWard2 = 7;

    void Start()
    {
        gears = GameObject.Find("GearsTest");
        player = gears.GetComponent<GearsScript>().player;
        player = GameObject.FindGameObjectWithTag("Player");
    }

    void Update()
    {
        playerInRange = Physics2D.OverlapCircle(transform.position, 3f, playerLayer);

        if (playerInRange)
        {
            if (Input.GetButtonDown("T"))
            {
                if (gears.GetComponent<GearsScript>().dayTime > timeWard1 || gears.GetComponent<GearsScript>().dayTime < timeWard2)
                {
                    gears.GetComponent<GearsScript>().dayTime = timeWard2;
                }
                else
                {
                    gears.GetComponent<GearsScript>().dayTime = timeWard1;
                }
            }
            
            //showTextToThePlayer.SetActive(true);
            ShowText();
            
            Vector2 textPos = Camera.main.WorldToScreenPoint(showTextHere.position);
            textInstantiated.transform.position = textPos;
        }
        else
        {
            //showTextToThePlayer.SetActive(false);
            Destroy(textInstantiated);
            textShowed = false;
        }
    }

    public void ShowText()
    {
        if (!textShowed)
        {
           GameObject textInstantiated2 = Instantiate(showTextToThePlayer);
           textInstantiated = textInstantiated2;
           textInstantiated.transform.SetParent(gears.GetComponent<GearsScript>().canvasMain.transform);
        }
        textShowed = true;
    }
}
