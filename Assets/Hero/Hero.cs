﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class Hero : MonoBehaviour
{
    public int life;

    public int damage;

    public bool isAlive;

    public SpriteRenderer playerSprite;

    public Rigidbody2D rb2D;

    public float speedMovement;
    
    // Apparence de base du héro
    private Color baseColor;
    
    // Start is called before the first frame update
    void Start()
    {
        isAlive = true;
        baseColor = playerSprite.color;
    }

    public void GetDamage(int _damage)
    {
        if (life > 0)
        {
            life -= _damage;
            TakeDamage();
        }
        else
        {
            isAlive = false;
        }
    }
    
    public void TakeDamage()
    {
        if (playerSprite.color == baseColor)
        {
            rb2D.AddForce(- transform.right * 100); // changer le "* 5000" par la puisssance dans un script du gameobject
            playerSprite.DOColor(Color.red, 1).OnComplete(ReturnBaseColor);
        }
    }

    public void ReturnBaseColor()
    {
        playerSprite.color = baseColor;
    }

    // Update is called once per frame
    void Update()
    {
        if (!isAlive)
        {
            // Mort (Game Over)
            Destroy(this);
        }
    }
}
